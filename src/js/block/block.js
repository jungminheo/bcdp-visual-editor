/**
 * 
 */
var Properties = {
	detailView : 'detailView' 
};

Properties.parameter = {
	addInputContainer : function(node) {
		// target
		var detailView = getEl(Properties.detailView);
		
		// input container
		var inputContainer = createEl('div');
		inputContainer.className = 'input-container';
		
		// title
		var title = createEl('span');
		title.className = 'container-title';
		title.innerHTML = 'Input Parameter';
		
		// input parameter list container 
		var listContainer = createEl('div');
		listContainer.className = 'list-container';
		
		// add button
		var addBtn = createEl('div');
		addBtn.className = 'btn btn-add';
		addBtn.innerHTML = 'ADD';
		addEvent(addBtn, 'click', function() {			
			var item = Properties.parameter.addInputParameter({type: 'output', nodeId: node.metaData.nodeId}, node);
			addEl(listContainer, item);
			addCustomFunction(node.metaData);
		});
		
		addEl(inputContainer, title);
		addEl(inputContainer, addBtn);
		addEl(inputContainer, listContainer);
		addEl(detailView, inputContainer);
		return listContainer;
	},
	addInputParameter : function(metaData, node) {
		var edge;
		if (metaData.portId && metaData.index != undefined) {
			edge = getEl(metaData.portId).parentNode;
		} else {
			edge = createOuputEdge(metaData);	
		}
		
		var index = metaData.index;
		var output = node.metaData.output;
		if (index == undefined) {
			for (var i = 0; i < output.length; i++) {
				if (output[i] == null) {
					index = i;
					break;
				}
			}
			if (index != undefined) {
				edge.metaData.index = index;
				node.metaData.output[index] = edge.metaData;
			} else {
				index = node.metaData.output.length;
				edge.metaData.index = index;
				node.metaData.output.push(edge.metaData);
			}			
			node.appendChild(edge);
		}
		
		if (!metaData.index) {
			node.metaData.edge[edge.metaData.portId] = edge.metaData;
		} 
		
		// div
		var container = createEl('div');
		container.className = 'container-item';
		container.node = node;
		container.edge = edge;
		
		// parameter label
		var label = createEl('input');
		label.type = 'text';
		label.className = 'input-label';
		label.value = metaData.name || 'Variable' + index;
		label.target = edge.metaData.portId;
		$(label).on('change', function() {
			edge.metaData.name = this.value;
			node.metaData.edge[edge.metaData.portId] = edge.metaData;
			node.metaData.output[edge.metaData.index] = edge.metaData;
			$(edge).find('.output-name').html(label.value);
			addCustomFunction(node.metaData);
		}).trigger('change');
		
		
		// detail
		var detail = createEl('div');
		
		// parameter type
		var type = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
		type.className = 'input-type';
		type.value = metaData.type || '';
		$(type).on('change', function() {
			detail.innerHTML = '';
			if (this.value == 'Array') {
				var itemType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				$(itemType).val(edge.metaData.baseType || 'Int').on('change', function() {
					edge.metaData.baseType = this.value;
				}).trigger('change');
				// type
				addEl(detail, Properties.parameter.addProperty('Item type', itemType));
				// dimension
				var dimension = createEl('span');
				var d1 = createEl('input');
				var d2 = createEl('input');
				var d3 = createEl('input');
				$(d1).attr('type', 'number').css('width', '50px').val(edge.metaData.dimension1 || 0).on('change', function() {
					edge.metaData.dimension1 = this.value;
				}).trigger('change');
				$(d2).attr('type', 'number').css('width', '50px').val(edge.metaData.dimension1 || 0).on('change', function() {
					edge.metaData.dimension2 = this.value;
				}).trigger('change');
				$(d3).attr('type', 'number').css('width', '50px').val(edge.metaData.dimension1 || 0).on('change', function() {
					edge.metaData.dimension3 = this.value;
				}).trigger('change');				
				addEl(dimension, d1);
				addEl(dimension, d2);
				addEl(dimension, d3);
				addEl(detail, Properties.parameter.addProperty('Dimension', dimension));
			} else if (this.value == 'Map') {
				var keyType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				$(keyType).val(edge.metaData.baseTypeKey || 'Int').on('change', function() {
					edge.metaData.baseTypeKey = this.value;
				}).trigger('change');
				addEl(detail, Properties.parameter.addProperty('Key type', keyType));
				var valueType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				$(valueType).val(edge.metaData.baseTypeValue || 'Int').on('change', function() {
					edge.metaData.baseTypeValue = this.value;
				}).trigger('change');
				addEl(detail, Properties.parameter.addProperty('Value type', valueType));
			}
			
			edge.metaData.type = type.value;
			node.metaData.edge[edge.metaData.portId] = edge.metaData;
			node.metaData.output[edge.metaData.index] = edge.metaData;
			addCustomFunction(node.metaData);
		}).trigger('change');
		
		
		// order change
		var orderWrap = createEl('div');
		orderWrap.className = 'order-wrap';
		
		// up arrow
		var up = createEl('div');
		up.className = 'order-up';
		addEvent(up, 'click', function() {
			index = edge.metaData.index;
			
			// change property
			var _container = this.parentNode.parentNode.parentNode;
			if (index == 0) {
				return;
			} else {
				// change property
				var _child = _container.children[index];
				var _targetChild = _container.children[index - 1];
				_container.insertBefore(_child, _targetChild);
				
				// change edge
				var _edgeParent = edge.parentNode;
				_edgeParent.insertBefore(edge, _targetChild.edge);
				edge.metaData.index -= 1;
				_targetChild.edge.metaData.index += 1;
				
				// change output meta data
				var _me = output[index];
				var _target = output[index - 1];
				
				// change
				_me.index = index - 1;
				_target.index = index;
				output[index - 1] = _me;
				output[index] = _target;				
			}			
		});
		
		// down arrow
		var down = createEl('div');
		down.className = 'order-down';
		addEvent(down, 'click', function() {
			index = edge.metaData.index;
			console.log(index, index - 1, index);
			var _container = this.parentNode.parentNode.parentNode;
			if (index == output.length - 1) {
				return;
			} else {
				// change property
				var _child = _container.children[index];
				var _targetChild = _container.children[index + 1];
				_container.insertBefore(_targetChild, _child);
				
				// change edge
				var _edgeParent = edge.parentNode;
				_edgeParent.insertBefore(_targetChild.edge, edge);
				edge.metaData.index += 1;
				_targetChild.edge.metaData.index -= 1;
				
				// change output meta data
				var _me = output[index];				
				var _target = output[index + 1];
				
				// change
				_me.index = index + 1;
				_target.index = index;
				output[index + 1] = _me;
				output[index] = _target;				
			}			
		});
		
		addEl(orderWrap, up);
		addEl(orderWrap, down);
		
		var delBtn = createEl('div');
		delBtn.className = 'btn btn-del';
		delBtn.innerHTML = 'DEL';
		addEvent(delBtn, 'click', function() {
			var parent = this.parentNode;
			// delete 
			parent.parentNode.removeChild(parent);
			node.metaData.edge[edge.metaData.portId] = null;
			node.metaData.output[edge.metaData.index] = null;
			removeEdgeNode(edge.metaData.portId);
		});
		addEl(container, label);
		addEl(container, type);
		addEl(container, orderWrap);
		addEl(container, delBtn);
		addEl(container, detail);
		return container;
	},
	addProperty : function(label, input) {
		var div = createEl('div');
		div.className = 'item-detail';
		var title = createEl('span');
		title.className = 'item-label';
		title.innerHTML = label;
		addEl(div, title);
		addEl(div, input);
		return div;
	}
};
