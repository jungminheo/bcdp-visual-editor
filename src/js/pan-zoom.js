$(document).ready(function(){
	// disable context menu (mouse right click event)
	//document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
	
	document.PanZoom = {};
	var PanZoom = document.PanZoom;

	PanZoom.currentScale = 1;
	PanZoom.maximumScale = 3;
	PanZoom.minimumScale = 0.5;
	
	window.__zoomScaleStack = 0.1;
	
	PanZoom.changeChildPosition = function(el, child, movementX, movementY) {
		$("#scaleDiv").html("X " + Math.round(PanZoom.currentScale*10)/10);
		// correction of scale
		var correctionX = child[0].clientWidth*(PanZoom.currentScale-1)/2;
		var correctionY = child[0].clientHeight*(PanZoom.currentScale-1)/2;
		// boundary condition	
		var wrapper = $(el)[0];
		var limitX = (child[0].clientWidth*(PanZoom.currentScale+1)/2 - wrapper.clientWidth)*(-1);
		var limitY = (child[0].clientHeight*(PanZoom.currentScale+1)/2 - wrapper.clientHeight)*(-1);
		// movement position
		var left = child.css('left').replace('px','')*1 + movementX;
		var top = child.css('top').replace('px','')*1 + movementY;
		
		// check boundary condition	
		if (left > correctionX ) {
			left = correctionX;
		} else if (left < limitX) {
			left = limitX;
		}
		if (top > correctionY ) {
			top = correctionY;
		} else if (top < limitY) {
			top = limitY;
		}
		child.css({
			'left' : left + 'px',
			'top' : top + 'px'
		});				
	}
	
	// zoom in / out
	PanZoom.zoom = function(angle, canvas) {
		$(angle)	
		.on('wheel', function(e){
			// mouse wheel event listener
			// mouse original event
			var oe = e.originalEvent;
			// get wheel delta y (vertical wheel scroll)
			var y = oe.deltaY;
			// check positive and negative
			var child = $(canvas);
			var upDown = 0;
			var o = e.originalEvent;
			var translateX = (child[0].clientWidth-2*o.offsetX)*__zoomScaleStack/2;
			var translateY = (child[0].clientHeight-2*o.offsetY)*__zoomScaleStack/2;
			if (y < 0) {
				// if positive then child scale up
				if (PanZoom.currentScale < PanZoom.maximumScale) {
					upDown = __zoomScaleStack;
					PanZoom.currentScale = PanZoom.currentScale + upDown;
					if (PanZoom.currentScale > PanZoom.maximumScale) {
						PanZoom.currentScale = PanZoom.maximumScale;
					}
					child.css({            			
						'transform': 'scale('+ PanZoom.currentScale +')'
					});
					PanZoom.changeChildPosition(this, child, translateX, translateY);
				}			
			} else if (y > 0) {
				// if negative then child scale down
				if (PanZoom.currentScale > PanZoom.minimumScale) {
					upDown = -__zoomScaleStack;
					PanZoom.currentScale = PanZoom.currentScale + upDown;
					if (PanZoom.currentScale < PanZoom.minimumScale) {
						PanZoom.currentScale = PanZoom.minimumScale;
					}
					child.css({            			
						'transform': 'scale('+ PanZoom.currentScale +')'
					});
					PanZoom.changeChildPosition(this, child, -translateX, -translateY);
				}
			}			
		})
	};
	
	// panning
	PanZoom.panning = function(angle, canvas) {
		$(angle)
		.on('mousemove', function(e){
			var oe = e.originalEvent;
			// check right button
			if (oe.button) {
				// child position
				var child = $(canvas);
				PanZoom.changeChildPosition(this, child, oe.movementX, oe.movementY);
			}
	    });	
	}
	
	PanZoom.panZoom = function(angle, canvas) {
		this.panning(angle, canvas);
		this.zoom(angle, canvas);
	}
});

