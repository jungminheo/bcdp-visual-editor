const fs = require('fs');
const process = require('child_process');

var customFunctions = {
	// key value
	// key : name
	// value : metaData
}

var stableVariables = {
	// key value
	// key : name
	// value : metaData
}
var currentValue = 0;

function addCustomFunction(metaData) {
	var data = {
		name : metaData.name,
		type : 'function',
		"fn" : metaData.name,
		source : [{ type : 'source'}],
		sink : [{ type: 'sink' }],
		input: [],
		output: [
			{
				name : "retrun"				
			}				
		]
	}
	if (metaData.output) {
		for (var i = 0; i < metaData.output.length; i++) {
			var outputData = metaData.output[i];
			if (outputData) {
				var input = {
					name : outputData.name,
					type : outputData.type
				}
				data.input.push(input);
			}
		}
	}

	if (!customFunctions[metaData.name]) {
		var item = addMenuItem(getEl("function").querySelector('.nav-subset'), data, 'function_' + metaData.name);
	}
	
	getEl('function_' + metaData.name).children[0].metaData = data;
	customFunctions[metaData.name] = data;
}

function addStableVariable(metaData) {
	if (metaData.variableName) {
		var data = {
			name : metaData.variableName,
			type : 'stableVariable',
			output : [{
				name : metaData.variableName,
				type : metaData.variableType
			}],
			dataType : metaData.variableType,
		}
		
		if (metaData.variableType == 'Array') {
			data.baseType = metaData.baseType;
		} else if (metaData.variableType == 'Map') {
			data.baseTypeKey = metaData.baseTypeKey;
			data.baseTypeValue = metaData.baseTypeValue;
		}
		
		if(!stableVariables[metaData.variableName]) {
			// create
			var item = addMenuItem(getEl("stableVariable").querySelector('.nav-subset'), data, 'variable_' + metaData.variableName);		
		} else {
			// update
			getEl('variable_' + metaData.variableName).children[0].metaData = data;		
		}
		stableVariables[metaData.variableName] = data;		
	}
}

function removeCustomFunction(name) {
	var functions = $("#canvas .function");
	var result = false;
	for (var i = 0; i < functions.length; i++) {
		var component = functions[i];
		if (component.metaData.name == name) {
			result = true;
			break;
		} 
	}
	
	if (!result) {
		delete customFunctions[name];
		var menu = getEl('function_' + name);
		if (menu) {
			menu.parentNode.removeChild(menu);	
		}		
	}
}

function readConfig() {
	var configFilePath = __dirname + '/config.json';
	fs.readFile(configFilePath, {encoding: "utf8"} , function (err, data){
		var data = JSON.parse(data);
		saveFilePath = data.filePath;
		compiler = data.compiler;        
    });
}

$(document).ready(function(){
	window.blockIdSequece_ = 1;
	window.edgeSequence_ = 1;
	
	var canvas = getEl("canvas");
	canvas.addEventListener("drop", (e) => {
		var t = e.dataTransfer;
		if (t.getData("type") == "create") {
			var block = createBlockComponent(getEl("canvas"), JSON.parse(e.dataTransfer.getData("metaData")), e.offsetX, e.offsetY);
			$(block).trigger('click');
			//changeDetail(block);
			e.preventDefault();		
		} else {
			e.preventDefault();
		}		
		return false;
	});
	
	canvas.addEventListener("dragover", (e) => {
		e.preventDefault();
		return false;
	});	
	$(document).on("keyup", (e) => {
		// check delete key
		if (e.keyCode == 46) {
			console.log(e.target.tagName, e.target.tagName == 'INPUT');
			if (e.target.tagName == 'INPUT') {
				return false;
			}
			removeActiveBlockComponent();
		}
	});
	
	readConfig();
});

function removeActiveBlockComponent() {
	var blockComponent = $(".block-component.active");
	if (blockComponent.length > 0) {
		$.each(blockComponent, (index, item) => {
			$(item).find(".input-edge, .output-edge, .input-event, .output-event").each((index, item)=>{
				var connections = item.connection;
				$(connections).each((a, b) => {
					deleteCurve({target: b});
				});
			});
			if (item.metaData.type == 'function') {
				removeCustomFunction(item.metaData.name);	
			}			
			$(item).remove();
		});
	}	
	changeDetail(null);
}

function draggableWithZoom(ui) {
	var zoomScale = document.PanZoom.currentScale;
	var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
	var changeTop = ui.position.top - ui.originalPosition.top; // find change in top

	// set grid line
	var interval = 30 * zoomScale;
	changeLeft = changeLeft - changeLeft%interval;
	changeTop = changeTop - changeTop%interval;
	
    var newLeft = ui.originalPosition.left + changeLeft / zoomScale; // adjust new left by our zoomScale
    var newTop = ui.originalPosition.top + changeTop / zoomScale; // adjust new top by our zoomScale
    
    ui.position.left = newLeft;
    ui.position.top = newTop;
    updateCanvasSize(newLeft, newTop);
}

function updateCanvasSize(newLeft, newTop) {
    var canvas = document.getElementById('canvas');
    if ((canvas.offsetWidth - newLeft) < 150) {
    	canvas.width = canvas.offsetWidth + 200;
    	canvas.style.width = canvas.offsetWidth + 200 + 'px';
    	document.PanZoom.changeChildPosition('#canvasView', $('#canvas'), 0, 0);
    }
    if ((canvas.offsetHeight - newTop) < 150) {
    	canvas.height = canvas.offsetHeight + 200;
    	canvas.style.height = canvas.offsetHeight + 200 + 'px';
    	console.log(canvas.height, canvas.offsetHeight);
    	document.PanZoom.changeChildPosition('#canvasView', $('#canvas'), 0, 0);
    }	
}

function createBlockComponent(container, metaData, posX, posY) {
	var item = createEl("div");

	// node id
	if (!metaData.nodeId) {
		metaData.nodeId = blockIdSequece_++;
	} else {
		if (blockIdSequece_ <= metaData.nodeId) {
			blockIdSequece_ = metaData.nodeId + 1;
		}
	}
	
	// set grid line
	var interval = 30;
	posX = posX - posX%interval;
	posY = posY - posY%interval;
	
	updateCanvasSize(posX, posY);
	
	// initialize
	item.id = metaData.nodeId;
	item.className = "block-component";
	item.style.left = posX + "px";
	item.style.top = posY + "px";
	item.style.position = 'absolute';
	$(item).addClass(metaData.type);
	
	// title
	var title = createEl("div");
	title.className = "block-title";
	if (metaData.type == "variable") {
		title.innerHTML = metaData.variableName || metaData.name;	
	} else if (metaData.type == "event" || metaData.type == "function") {
		title.innerHTML = (metaData.variableName || metaData.name);	
	} else {
		title.innerHTML = metaData.title || metaData.name;
	}	
	title.title = metaData.tooltip;	
	addEl(item, title);
	
	// drag
	$(item).draggable({
		cursor: "move",
		start: function(event, ui) {
			ui.position.left = 0;
			ui.position.top = 0;		
		},
		drag: function (e, ui) {			
			$(e.target).find(".input-container.active .input-edge, .output-container.active .output-edge,.input-event-container.active .input-event, .output-event-container.active .output-event").each((index, item)=>{
				var connections = item.connection;
				for (var connection in connections) {
					var node = connections[connection];
					$(node).trigger("update");
				}
			});
			draggableWithZoom(ui);
		}
	});
	
	// active
	$(item).on("click", (e) => {
		var active = $(".block-component.active");
		var target = e.currentTarget;
		if (active && active.length > 0) {
			if(active[0] != target) {
				active.removeClass("active");
				$(target).addClass("active");
			}
		} else {
			$(target).addClass("active");
		}		
		changeDetail(target);
	}).on('dblclick', function(event) {
		Editor.toggleBreakpoint(item);
	});
	
	item.metaData = metaData;
	addEl(container, item);	
	metaData.edge = {};
	metaData.outEventEdge = [];
	
	// source
	if (metaData.source instanceof Array) {
		for (var i = 0; i < metaData.source.length; i++) {
			var source = metaData.source[i];
			var edge = createEventEdge('input', source, item);
			item.metaData.source[i] = edge.metaData;
		}
	}
	
	// sink
	if (metaData.sink instanceof Array) {
		for (var i = 0; i < metaData.sink.length; i++) {
			var sink = metaData.sink[i];
			var edge = createEventEdge('output', sink, item);
			metaData.outEventEdge.push(edge.metaData);
			item.metaData.sink[i] = edge.metaData;
		}
	}
	
	// separation
	var sepa = createEl('div');
	sepa.className = 'separator';
	addEl(item, sepa);
	
	// create parameter node
	// input parameter
	var inputArr = metaData.input;
	if (inputArr instanceof Array) {
		var inputDiv = createEl("div");
		inputDiv.className = "input";
		for (var i = 0; i < inputArr.length; i++) {
			inputArr[i].nodeId = metaData.nodeId;
			var edge = createInputEdge(inputArr[i]);
			addEl(inputDiv, edge);
			// replace metaData
			item.metaData.input[i] = edge.metaData;
			metaData.edge[edge.metaData.portId] = edge.metaData;
		}
		addEl(item, inputDiv);
	}	
	
	// create parameter node
	// output parameter
	if (metaData.type == "variable") {
		// output edge
		if (metaData.output && metaData.output.length != 0) {
			metaData.output = [{
				name: metaData.output[0].name,
				type: "output"
			}];
		} else {
			metaData.output = [{				
				type: "output"
			}];
		}		
	}
	
	var outputArr = metaData.output;
	if (outputArr instanceof Array) {
		var outputDiv = createEl("div");
		outputDiv.className = "output";
		for (var i = 0; i < outputArr.length; i++) {
			if (outputArr[i]) {
				outputArr[i].nodeId = metaData.nodeId;
				var	edge = createOuputEdge(outputArr[i]);	
				addEl(outputDiv, edge);
				// replace metaData
				item.metaData.output[i] = edge.metaData;
				metaData.edge[edge.metaData.portId] = edge.metaData;				
			}
		}
		addEl(item, outputDiv);
	}	
	
	if (metaData.type == 'customFunction') {
		addCustomFunction(item.metaData);	
	}
	
	if (metaData.type == 'variable') {
		addStableVariable(item.metaData);
	}
	
	return item;
}

function createEventEdge(type, edgeData, block) {
	var el = createEl('div');
	el.className = type + ' event-edge';
	var edge;
	if (type == "input") {
		edge = createInputEvent(edgeData);
		edge.metaData['type'] = 'sink';
	} else {
		edge = createOutputEvent(edgeData);
		edge.metaData['type'] = 'source';
	}
	addEl(el, edge);
	addEl(block, el);
	edge.metaData['nodeId'] = block.metaData.nodeId;
	block.metaData.edge[edge.metaData.portId] = edge.metaData;
	return edge;
}


function addTooltip(el) {
	$(el).on("mouseover", (e) => {
		var t = e.currentTarget;
		var m = t.metaData.tooltip;
		if (m) {
			var p = t.offsetParent;
			showTooltip(p.offsetLeft + p.offsetWidth, p.offsetTop + p.offsetHeight, m);
		}		
		return false;
	}).on("mouseout", hideTooltip);
	
}
var tempCurve = null;
function createInputEdge(metaData) {
	var container = createEl("div");
	container.className = "input-container";
	// edge
	var edge = createEl("div");	
	edge.className = "input-edge";
	
	edge.connection = [];
	var portId = metaData.portId;
	if (portId) {
		edge.id = portId;
		var sequence = portId.replace("edge","") * 1;
		if (edgeSequence_ <= sequence) {
			edgeSequence_ = sequence + 1;
		}	
	} else {
		portId = "edge" + edgeSequence_ ++;
		edge.id = portId;
		metaData.portId = portId;
	}
	edge.metaData = metaData;
	container.metaData = metaData;
	addEl(container, edge);	
	
		
	// drag
	$(edge).draggable({
		cursor: "auto", 
		helper: "clone",		
	    appendTo: "#canvas",
	    start: function(event, ui) {
			ui.position.left = 0;
		    ui.position.top = 0;
		    $(ui.helper.context.parentNode).addClass("drag");
		},
		stop: function(event, ui) {
			if (tempCurve) {
				$(tempCurve).remove();
				tempCurve = null;	
			}
			$(ui.helper.context.parentNode).removeClass("drag");
		},
	    drag: function(e, ui) {
    		if (!tempCurve) {
    			tempCurve = createSimpleCurve();
    		}    		
    		var t = ui.helper.context;
    		var p = ui.helper.context.offsetParent;    		
    		var scale = document.PanZoom.currentScale;
    		ui.position.left /= scale;
    		ui.position.top /= scale;
    		var path = makeCurveStringPath(
    				ui.position.left + t.offsetWidth/2, 
    				ui.position.top + t.offsetHeight/2, 
    				p.offsetLeft + t.offsetLeft + t.offsetWidth/2, 
    				p.offsetTop + t.offsetTop + t.offsetHeight/2
			);
			tempCurve.setAttribute("d", path);	
	    }
	})
	
	// drop event
	$(edge).droppable({
		accept: ".output-edge",
		drop: function(e, ui) {
			createCurve(ui.draggable[0].id, e.target.id, 'param');
		}
    });
	
	// name
	var name = createEl("span");
	name.className = "input-name";
	name.innerHTML = metaData.name;
	addEl(container, name);
	// default value
	var value = metaData.value;
	var t = metaData.type;
	var input = createEl("input");
	input.className = "input-value"
	if (t == "boolean") {
		// boolean check box
		input.className = "input-value checkbox"
		input.type = "checkbox";
		input.value = (value)? value:false;		
	} else if (t == "text"){
		input.type = "text";
		input.value = (value)? value:"";		
	} else if (t == "int" || t == "float"){
		input.type = "number";
		input.value = (value)? value:"";		
	} else {
		input.type = "text";
		input.value = (value)? value:"";
		edge.metaData
	}
	$(input).on('change', function() {
		edge.metaData.value = input.value;
	});
	
	addEl(container, input);	
	edge.title = metaData.tooltip;
	return container;
}

function createOuputEdge(metaData) {
	var container = createEl("div");
	container.className = "output-container";
	// name
	var name = createEl("span");
	name.className = "output-name";
	name.innerHTML = (metaData.name)? metaData.name: "";
	addEl(container, name);
	// edge
	var edge = createEl("div");	
	edge.className = "output-edge";
	edge.connection = [];
	
	var portId = metaData.portId;
	if (portId) {
		edge.id = portId;
		var sequence = portId.replace("edge","") * 1;
		if (edgeSequence_ <= sequence) {
			edgeSequence_ = sequence + 1;
		}	
	} else {
		portId = "edge" + edgeSequence_ ++;
		edge.id = portId;
		metaData.portId = portId;
	}
	edge.metaData = metaData;	
	container.metaData = metaData;
	addEl(container, edge);	
	
	// drag
	$(edge).draggable({
		cursor: "auto", 
		helper: "clone",		
	    appendTo: "#canvas",
	    start: function(event, ui) {
			ui.position.left = 0;
		    ui.position.top = 0;
		    $(ui.helper.context.parentNode).addClass("drag");
		},
		stop: function(event, ui) {
			if (tempCurve) {
				$(tempCurve).remove();
				tempCurve = null;	
			}
			$(ui.helper.context.parentNode).removeClass("drag");
		},
	    drag: function(e, ui) {
    		if (!tempCurve) {
    			tempCurve = createSimpleCurve();
    		}    		
    		var t = ui.helper.context;
    		var p = ui.helper.context.offsetParent;    
    		var scale = document.PanZoom.currentScale;
    		ui.position.left /= scale;
    		ui.position.top /= scale;
    		var path = makeCurveStringPath(
    				p.offsetLeft + t.offsetLeft + t.offsetWidth/2, 
    				p.offsetTop + t.offsetTop + t.offsetHeight/2,
    				ui.position.left + t.offsetWidth/2, 
    				ui.position.top + t.offsetHeight/2 
			);
			tempCurve.setAttribute("d", path);	
	    }
	})
	
	// drop event
	$(edge).droppable({
		accept: ".input-edge",
		drop: function(e, ui) {
			createCurve(e.target.id, ui.draggable[0].id, 'param');
		}
    });
	addTooltip(edge);
	return container;
}
function createInputEvent(metaData) {
	var container = createEl("div");
	container.className = "input-event-container";

	// edge
	var edge = createEl("div");	
	edge.className = "input-event";
	edge.connection = [];
	var portId = metaData.portId;
	if (portId) {
		edge.id = portId;
		var sequence = portId.replace("edge","") * 1;
		if (edgeSequence_ <= sequence) {
			edgeSequence_ = sequence + 1;
		}	
	} else {
		portId = "edge" + edgeSequence_ ++;
		edge.id = portId;		
		metaData.portId = portId;
	}
	edge.metaData = metaData;
	container.metaData = metaData;
	addEl(container, edge);
	
	// name
	var name = createEl("span");
	name.className = "edge-name";
	name.innerHTML = metaData.name || "";
	addEl(container, name);
	
	// drag
	$(edge).draggable({
		cursor: "auto", 
		helper: "clone",		
	    appendTo: "#canvas",
	    start: function(event, ui) {
			ui.position.left = 0;
		    ui.position.top = 0;
		    $(ui.helper.context.parentNode).addClass("drag");
		},
		stop: function(event, ui) {
			if (tempCurve) {
				$(tempCurve).remove();
				tempCurve = null;	
			}
			$(ui.helper.context.parentNode).removeClass("drag");
		},
	    drag: function(e, ui) {
    		if (!tempCurve) {
    			tempCurve = createSimpleCurve();
    		}    		
    		var t = ui.helper.context;
    		var p = ui.helper.context.offsetParent;    	
    		var scale = document.PanZoom.currentScale;
    		ui.position.left /= scale;
    		ui.position.top /= scale;
    		var path = makeCurveStringPath(
    				ui.position.left + t.offsetWidth/2, 
    				ui.position.top + t.offsetHeight/2,
    				p.offsetLeft + t.offsetLeft + t.offsetWidth/2, 
    				p.offsetTop + t.offsetTop + t.offsetHeight/2
			);
			tempCurve.setAttribute("d", path);	
	    }
	})
	
	// drop event
	$(edge).droppable({
		accept: ".output-event",
		tolerance: "pointer",
		drop: function(e, ui) {
			createCurve(ui.draggable[0].id, e.target.id, 'event');
		},
		over: (e, ui) => {
			var x = e.clientX, y = e.clientY;
			showTooltip(e.clientX, e.clientY, null);			
		},
		out: hideTooltip,
		deactivate: hideTooltip
    });	
	addTooltip(edge);
	return container;
}
function createOutputEvent(metaData) {
	var container = createEl("div");
	container.className = "output-event-container";

	// name
	var name = createEl("span");
	name.className = "edge-name";
	name.innerHTML = metaData.name || "";
	addEl(container, name);
	
	// edge
	var edge = createEl("div");	
	edge.className = "output-event";
	edge.connection = [];
	var portId = metaData.portId;
	if (portId) {
		edge.id = portId;
		var sequence = portId.replace("edge","") * 1;
		if (edgeSequence_ <= sequence) {
			edgeSequence_ = sequence + 1;
		}	
	} else {
		portId = "edge" + edgeSequence_ ++;
		edge.id = portId;
		metaData.portId = portId;
	}
	edge.metaData = metaData;
	container.metaData = metaData;
	addEl(container, edge);	
	
	// drag
	$(edge).draggable({
		cursor: "auto", 
		helper: "clone",		
	    appendTo: "#canvas",
	    start: function(event, ui) {
			ui.position.left = 0;
		    ui.position.top = 0;
		    $(ui.helper.context.parentNode).addClass("drag");
		},
		stop: function(event, ui) {
			if (tempCurve) {
				$(tempCurve).remove();
				tempCurve = null;	
			}
			$(ui.helper.context.parentNode).removeClass("drag");
		},
	    drag: function(e, ui) {
    		if (!tempCurve) {
    			tempCurve = createSimpleCurve();
    		}    		
    		var t = ui.helper.context;
    		var p = ui.helper.context.offsetParent;    		
    		var scale = document.PanZoom.currentScale;
    		ui.position.left /= scale;
    		ui.position.top /= scale;
    		var path = makeCurveStringPath(
    				p.offsetLeft + t.offsetLeft + t.offsetWidth/2, 
    				p.offsetTop + t.offsetTop + t.offsetHeight/2,
    				ui.position.left + t.offsetWidth/2, 
    				ui.position.top + t.offsetHeight/2
			);
			tempCurve.setAttribute("d", path);	
	    }
	})
	
	// drop event
	$(edge).droppable({
		accept: ".input-event",
		tolerance: "pointer",
		drop: function(e, ui) {
			createCurve(e.target.id, ui.draggable[0].id, 'event');
		},
		over: (e, ui) => {
			var x = e.clientX, y = e.clientY;
			showTooltip(e.clientX, e.clientY, null);			
		},
		out: hideTooltip,
		deactivate: hideTooltip
    });	
	addTooltip(edge);
	return container;
}
// Tool tip
function showTooltip(x, y, text) {
	text = text || "Tooltip!!!";
	var tooltip = getEl("tooltip");
	tooltip.style.top = y + 'px';
	tooltip.style.left = x + 'px';	
	$(tooltip).find("p").html(text);
	$(tooltip).css("display", "block");
}
function hideTooltip() {
	$(getEl("tooltip")).css("display", "none");
}
// curve
function createCurve(startId, finishId, type) {
	var startEl = getEl(startId);
	var finishEl = getEl(finishId);
	if (!startEl || !finishEl) {
		return;
	}
	var pathEl = createEl("path");
	pathEl.setAttribute("fill","none");
	pathEl.setAttribute("stroke", "orange");
	pathEl.setAttribute("stroke-width", "2px");
	pathEl.startId = startId;
	pathEl.finishId = finishId;	
	pathEl.type = type;
	addEl(getEl("canvasSvg"), pathEl);

	// fill edge
	startEl.connection.push(pathEl);
	finishEl.connection.push(pathEl);
	$(startEl.parentNode).addClass("active");
	$(finishEl.parentNode).addClass("active");
	
	// update trigger
	$(pathEl).on("update", updateCurve);
	$(pathEl).trigger("update");
	
	// delete trigger
	$(pathEl).on("click", deleteCurve);
}
function updateCurve(e) {
	var line = e.target;
	//	start edge
	var edge = getEl(line.startId) || line.startId;
	var box = edge.offsetParent;
	console.log(box.offsetLeft, box.offsetTop);
	var startX = box.offsetLeft + edge.offsetLeft + edge.offsetWidth/2;
	var startY = box.offsetTop + edge.offsetTop + edge.offsetHeight/2;
	
	// finish edge
	var target = getEl(line.finishId) || line.finishId;
	var tBox = target.offsetParent;
	var finishX = tBox.offsetLeft + target.offsetLeft + target.offsetWidth/2;
	var finishY = tBox.offsetTop + target.offsetTop + target.offsetHeight/2;
	$(line).attr("d", makeCurveStringPath(startX, startY, finishX, finishY));
}
function createSimpleCurve() {
	var pathEl = createEl("path");
	pathEl.setAttribute("fill","none");
	pathEl.setAttribute("stroke", "orange");
	pathEl.setAttribute("stroke-width", "2px");	
	addEl(getEl("canvasSvg"), pathEl);
	return pathEl;
}
function makeCurveStringPath(startX, startY, finishX, finishY) {
	// border add
	startX += 2;
	startY += 2;
	finishX += 2;
	finishY += 2;
	
	// center point
	var cpX = startX + (finishX-startX)/2;
	var cpY = startY + (finishY-startY)/2;
	
	// approach point
	var apX, apY;
	if (finishX < startX) {
		apX = startX + 50;
		if (finishY < startY) {
			apY = startY - 20;
		} else {
			apY = startY + 20;	
		}		
	} else {
		apX = cpX;	
		apY = startY;
	}

	var path;
	path = "M " + startX + " " + startY + " ";
	path += "Q " + apX + " " + apY + " ";
	path += cpX + " " + cpY + " ";
	path += "T " + finishX + " " + finishY + "";
	return path;
}
function deleteCurve(e) {
	var item = e.target;
	var startEl = getEl(item.startId) || item.startId;
	var finishEl = getEl(item.finishId) || item.finishId;
	// array remove
	var startElConnection = startEl.connection;
	var finishElConnection = finishEl.connection;
	startElConnection.splice(startElConnection.indexOf(item), 1);
	finishElConnection.splice(finishElConnection.indexOf(item), 1);
	// check active
	if (startElConnection.length == 0) {
		$(startEl.parentNode).removeClass("active");
	};
	if (finishElConnection.length == 0) {
		$(finishEl.parentNode).removeClass("active");
	};
	item.remove();
}

function createPropertyLabel(label) {
	// label
	var labelEl = createEl('div');
	labelEl.className = 'detail-label';
	labelEl.innerHTML = label;
	return labelEl;
}

function createProperty(label, inputs, btnCallback) {
	var tr = createEl('tr');
	var labelTd = createEl('td');
	labelTd.appendChild(createPropertyLabel(label));
	tr.appendChild(labelTd);
		
	var target;
	if (inputs instanceof Array) {
		for (var i = 0; i < inputs.length; i++) {
			var propTd = createEl('td');
			propTd.appendChild(inputs[i]);
			if (target) {
				target = input[i].target;
			}
			addEl(tr, propTd);			
		}		
	} else {
		var propTd = createEl('td');
		propTd.colspan = 3;
		propTd.appendChild(inputs);
		target = inputs.target;
		addEl(tr, propTd);
	}	
	
	// delete
	if (btnCallback) {
		var buttonTd = createEl('td');
		var button = createEl('button');
		button.innerHTML = '-';
		button.style.color = 'red';
		button.style.fontWeight = 'bold';
		$(button).on('click', btnCallback);
		addEl(buttonTd, button);
		addEl(tr, buttonTd);
	}		
	return tr;
}

function removeEdgeNode(id) {
	var edge = getEl(id);
	// remove connections
	var connections = edge.connection;
	$(connections).each((a, b) => {
		deleteCurve({target: b});
	});
	// remove container
	var container = getEl(id).parentNode;			
	container.remove();
}
var listContainer = null;
function changeDetail(el) {
	var detailContainer = getEl('detailView');
	detailContainer.innerHTML = '';

	if (!el) { 
		return; 
	}

	var data = el.metaData;
	// properties
	// tool tip
//	var tooltip = getEl("detail-tooltip");
//	$(tooltip).html(data.tooltip);	

	if (data) {
		if (data.type == 'stableVariable') {
			// variable name
			var name = createEl('input');
			name.type = 'text';
			name.className = 'detail-field';
			name.setAttribute('readonly', true);
			name.value = el.metaData.name;
			
			// variable type
			var type = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
			type.className = 'detail-field';
			type.value = el.metaData.dataType;
			type.setAttribute('disabled', true);
			
			// detail
			var detail = createEl('div');
			if (type.value == 'Array') {
				var itemType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				itemType.value = el.metaData.baseType;
				itemType.setAttribute('disabled', true);
				addEl(detail, Properties.parameter.addProperty('Item type', itemType));
				
			} else if (type.value == 'Map') {
				var keyType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				keyType.value = el.metaData.baseTypeKey;
				keyType.setAttribute('disabled', true);
				
				var valueType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
				valueType.value = el.metaData.baseTypeValue;
				valueType.setAttribute('disabled', true);
				
				addEl(detail, Properties.parameter.addProperty('Key type', keyType));
				addEl(detail, Properties.parameter.addProperty('Value type', valueType));
			}

			addEl(detailContainer, Properties.parameter.addProperty('Variable', name));
			addEl(detailContainer, Properties.parameter.addProperty('Type', type));
			addEl(detailContainer, detail);
		}
		
		if (data.type == 'variable') {
			// variable name
			var name = createEl('input');
			name.type = 'text';
			name.className = 'detail-field';
			$(name).on('change', function() {
				el.querySelector('.block-title').innerHTML = name.value;
				el.metaData.variableName = name.value;
				el.metaData.output[0].name = name.value;
				addStableVariable(el.metaData);
			})
			if (el.metaData.variableName) {
				name.value = el.metaData.variableName;
				$(name).trigger('change');
			}
			
			// detail
			var detail = createEl('div');
			
			// input container
			var inputContainer = createEl('div');
			inputContainer.className = 'input-container';
			
			// title
			var title = createEl('span');
			title.className = 'container-title';
			title.innerHTML = 'Initialize';
			
			// input parameter list container 
			listContainer = createEl('div');
			listContainer.className = 'list-container';
			
			var type; 
			
			// add button
			var addBtn = createEl('div');
			addBtn.className = 'btn btn-add';
			addBtn.innerHTML = 'ADD';
			
			function addMapKeyValue(key, value) {
				var item = createEl('div');
				item.className = 'container-item';
				// key
				var keyRef = createEl('input');
				keyRef.type = 'text';
				keyRef.value = key;
				// value
				var valueRef = createEl('input');
				valueRef.type = 'text';
				valueRef.value = value;
				addEvent(valueRef, 'change', function() {
					if (valueRef.value) {
						el.metaData.initValue[keyRef.value] = valueRef.value;	
					}
				});
				addEl(item, Properties.parameter.addProperty('Key', keyRef));
				addEvent(valueRef, 'change', function() {
					if (keyRef.value) {
						el.metaData.initValue[keyRef.value] = valueRef.value;	
					}
				});
				addEl(item, Properties.parameter.addProperty('Value', valueRef));
				addEl(listContainer, item);				
			}
			
			addEvent(addBtn, 'click', function() {				
				addMapKeyValue(null, null);
			});
			
			addEl(inputContainer, title);
			addEl(inputContainer, addBtn);
			addEl(inputContainer, listContainer);
			
			function createArrayInitialize(x, y, z) {
				x *= 1;
				y *= 1;
				z *= 1;
				console.log(x, y, z);
				// initialize
				listContainer.innerHTML = '';
				var _x = x, _y = y, _z = z;
				if (x == 0) {
					return;
				}
				
				if (y == 0) {
					_y = 1;
				}
				
				if (z == 0) {
					_z = 1;
				}
				var _dimension = 1;				
				// create data
				var data;
				if (z != 0) {
					// 3 dimension
					data = new Array(z);
					_dimension = 3;
				} else if (y != 0) {
					// 2 dimension
					data = new Array(y);
					_dimension = 2;
				} else {
					// 1 dimension
					data = new Array(x);
				}
				if (!el.metaData.initValue || (el.metaData.dimension1 != x || el.metaData.dimension2 != y || el.metaData.dimension3 != z)) {
					// diff change
					for (var i = 0; i < _z; i ++) {
						if (_dimension == 3) {
							data[i] = new Array(y);
						}
						for (var j = 0; j < _y; j ++) {
							if (_dimension == 2) {
								data[j] = new Array(x);
							} else if (_dimension == 3) {
								data[i][j] = new Array(x);
							}
						}
					}
					el.metaData.initValue = data;
				} 	
				
				for (var i = 0; i < _z; i ++) {
					var _container = createEl('div');
					_container.className = 'container-item';
					addEl(listContainer, _container);
					console.log(x, _y, _z);
					console.log(listContainer.children.length, _container);
					for (var j = 0; j < _y; j ++) {
						var row = createEl('div');
						row.className = 'item-detail';
						for (var k = 0; k < x; k++) {
							var column = createEl('input');
							column.type = 'text';
							var value = null;
							console.log(_dimension);
							if (_dimension == 3) {
								column.value = el.metaData.initValue[i][j][k] || null;
							} else if (_dimension == 2) {
								column.value = el.metaData.initValue[j][k] || null;
							} else {
								column.value = el.metaData.initValue[k] || null;
							}
							column.x = k;
							column.y = j;
							column.z = i;
							$(column).on('change', function() {
								if (_dimension == 3) {
									el.metaData.initValue[this.z][this.y][this.x] = this.value;
								} else if (_dimension == 2) {
									el.metaData.initValue[this.y][this.x] = this.value;
								} else {
									el.metaData.initValue[this.x] = this.value;
								}								
							}).trigger('change');
							addEl(row, column);
						}
						addEl(_container, row);
					}
				}
			}
			
			// variable type
			type = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
			type.className = 'detail-field';
			type.value = el.metaData.variableType || 'Int';
			$(type).on('change', function() {
				if (el.metaData.variableType && el.metaData.variableType != this.value) {
					// reset
					el.metaData.initValue = null;
					delete el.metaData.baseType;
					delete el.metaData.baseTypeKey;
					delete el.metaData.baseTypeValue;
				}
				el.metaData.variableType = this.value;
				detail.innerHTML = '';
				if (this.value == 'Array') {
					var itemType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
					$(itemType).val(el.metaData.baseType || 'Int').on('change', function() {
						el.metaData.baseType = this.value;
						addStableVariable(el.metaData);
					}).trigger('change');
					// type
					addEl(detail, Properties.parameter.addProperty('Item type', itemType));
					// dimension
					var dimension = createEl('span');
					var d1 = createEl('input');
					var d2 = createEl('input');
					var d3 = createEl('input');
					$(d1).attr('type', 'number').css('width', '50px').val(el.metaData.dimension1 || 0).on('change', function() {
						createArrayInitialize(d1.value, d2.value, d3.value);
						el.metaData.dimension1 = this.value;
						addStableVariable(el.metaData);
					});
					$(d2).attr('type', 'number').css('width', '50px').val(el.metaData.dimension2 || 0).on('change', function() {
						createArrayInitialize(d1.value, d2.value, d3.value);
						el.metaData.dimension2 = this.value;
						addStableVariable(el.metaData);
					});
					$(d3).attr('type', 'number').css('width', '50px').val(el.metaData.dimension3 || 0).on('change', function() {
						createArrayInitialize(d1.value, d2.value, d3.value);
						el.metaData.dimension3 = this.value;
						addStableVariable(el.metaData);
					}).trigger('change');				
					addEl(dimension, d1);
					addEl(dimension, d2);
					addEl(dimension, d3);
					addEl(detail, Properties.parameter.addProperty('Dimension', dimension));
					$(inputContainer).show();
					$(addBtn).hide();
				} else if (this.value == 'Map') {
					listContainer.innerHTML = '';
					if (!el.metaData.initValue) {
						console.log(el.metaData.initValue);
						el.metaData.initValue = {};
					} else {
						for (var key in el.metaData.initValue) {addMapKeyValue(key, el.metaData.initValue[key])};
					}					
					var keyType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
					$(keyType).val(el.metaData.baseTypeKey || 'Int').on('change', function() {
						el.metaData.baseTypeKey = this.value;
						addStableVariable(el.metaData);
					}).trigger('change');
					addEl(detail, Properties.parameter.addProperty('Key type', keyType));
					var valueType = createSelectOption(['Int', 'String', 'Boolean', 'Array', 'Map']);
					$(valueType).val(el.metaData.baseTypeValue || 'Int').on('change', function() {
						el.metaData.baseTypeValue = this.value;
						addStableVariable(el.metaData);
					}).trigger('change');
					addEl(detail, Properties.parameter.addProperty('Value type', valueType));
					$(inputContainer).show();
					$(addBtn).show();
				} else {					
					var init = createEl('input');
					init.type = 'text';
					init.className = 'detail-field';
					init.value = el.metaData.initValue || null;
					$(init).on('change', function() {
						el.metaData.initValue = init.value;
						addStableVariable(el.metaData);
					});
					addEl(detail, Properties.parameter.addProperty('Initialize', init));
					$(inputContainer).hide();
				}
				addStableVariable(el.metaData);
			});			
			
			addEl(detailContainer, Properties.parameter.addProperty('Variable', name));
			addEl(detailContainer, Properties.parameter.addProperty('Type', type));
			addEl(detailContainer, detail);
			addEl(detailContainer, inputContainer);
			$(type).trigger('change');
		}
		
		if (data.type == 'customFunction') {
			var name = createEl('input');
			name.type = 'text';
			name.className = 'detail-field';
			name.value = el.metaData.name;
			$(name).on('change', function() {
				el.querySelector('.block-title').innerHTML = name.value;
				removeCustomFunction(el.metaData.name);
				el.metaData.name = name.value;
				addCustomFunction(el.metaData);
				
				$(el).find(".input-container.active .input-edge, .output-container.active .output-edge,.input-event-container.active .input-event, .output-event-container.active .output-event").each((index, item)=>{
					var connections = item.connection;
					for (var connection in connections) {
						var node = connections[connection];
						$(node).trigger("update");
					}
				});
			})			
			addEl(detailContainer, Properties.parameter.addProperty('Name', name));
			var listContainer = Properties.parameter.addInputContainer(el);
			
			// check output
			if (!el.metaData.output) {
				el.metaData.output = [];	
			}	
			
			for (var i = 0; i < el.metaData.output.length; i++) {
				if (el.metaData.output[i]) {
					var item = Properties.parameter.addInputParameter(el.metaData.output[i], el);
					addEl(listContainer, item);	
				}				
			}	
		}
	}
}

function createInputParameter(metaData, table, node) {
	var edge;
	if (metaData.portId && metaData.index != undefined) {
		edge = getEl(metaData.portId).parentNode;
	} else {
		edge = createOuputEdge(metaData);	
	}
					
	var inputVariable = createEl('input');
	inputVariable.type = 'text';
	inputVariable.className = 'detail-field';
	inputVariable.value = metaData.name || '';
	inputVariable.target = edge.metaData.portId;
	$(inputVariable).on('change', function() {
		edge.metaData.name = inputVariable.value;
		node.metaData.edge[edge.metaData.portId] = edge.metaData;
		node.metaData.output[edge.metaData.index] = edge.metaData;
		$(edge).find('.output-name').html(inputVariable.value);
		addCustomFunction(node.metaData);
	});
	
	var type = createSelectOption(['int', 'string', 'boolean']);
	type.className = 'detail-field';
	type.value = metaData.type || '';
	$(type).on('change', function() {
		edge.metaData.type = type.value;
		node.metaData.edge[edge.metaData.portId] = edge.metaData;
		node.metaData.output[edge.metaData.index] = edge.metaData;
		addCustomFunction(node.metaData);
	});
	
	var index = metaData.index;
	if (index == undefined) {
		index = node.metaData.output.length + 1;
		edge.metaData.index = index;
		node.metaData.output.push(edge.metaData);
		node.appendChild(edge);
	}
	var prop = createProperty('Variable' + index, [inputVariable, type], function() {
		node.metaData.edge[edge.metaData.portId] = null;
		node.metaData.output[edge.metaData.index] = null;
		table.removeChild(prop);
		removeEdgeNode(edge.metaData.portId);
	})
	addEl(table, prop);		
	
	if (metaData.index != undefined) {
		node.metaData.edge[edge.metaData.portId] = edge.metaData;
	} 
}
//
//function createProperties(option, label) {
//	// container
//	var labelContainer = getEl("detail-label-container");
//	var fieldContainer = getEl("detail-field-container");
//	
//	// inputs
//	for (var i = 0; i < option.properties.length; i++) {
//		// label
//		var labelEl = createEl('div');
//		labelEl.className = 'detail-label';
//		labelEl.innerHTML = option.label;
//		labelContainer.appendChild(labelEl);
//	}
//	// input
//	labelContainer.appendChild(
//		$(createEl("div")).addClass('detail-label').html(label);
//	);
//	labelContainer.appendChild(
//		$(createEl("div")).addClass('detail-label').html(label);
//	);
//	
//}

function save() {
	// block component
	var blockComponentArray = [];
	$("#canvas .block-component").each((index, item) => {
		var metaData = item.metaData;
		metaData.offsetLeft = item.offsetLeft;
		metaData.offsetTop = item.offsetTop;
		if (metaData.isBreakpoint) {
			metaData.breakIndex = Editor.breakpoints.indexOf(item.id);
		}
		blockComponentArray.push(metaData);
	});
	
	//connection 
	var connectionArray = [];
	$("#canvas path").each((index, item) => {
		var start = item.startId;
		var finish = item.finishId;	
		var metaData = {
				start: start,
				finish: finish,
				type: item.type
		}
		connectionArray.push(metaData);
	});
	
	var saveData = {
			node : blockComponentArray,
			connections: connectionArray
	}
	return saveData;	
}

function compile() {
	var path = window.currentFileName;
	if (!path) {
		saveAsFile(compile);
		return;
	}
	
	var filePath = path.substring(0, path.lastIndexOf("\\"));
	var fileName = path.substring(path.lastIndexOf("\\"), path.lastIndexOf('.'));
	var code = generateCode(fileName);
	
	console.log(filePath, fileName);

	// save scheme code
	var currentCodePath = filePath + fileName + '.scm';	
	fs.writeFile(currentCodePath, code, {encoding: "utf8"});

	// compile test11
	var compiler = __dirname + '/scm/';
	compiler = compiler.replace(/\\/g, '/');
	console.log(compiler, currentCodePath);

	// shifter.exe -c %1.scm
	process.execFile("shifter.exe", ["-c", currentCodePath], {cwd: compiler}, (err, stdout, stderr) => {
		console.log(err, stdout, stderr);
	});	
}

function showConsole(message, reset) {
	var html = (reset)? $('.console-pre').html(): "";
	html += message + "\r\n";
	// tab to white space
	html.replace(/\t/g, "    ");
	$('.console-pre').html(html);
}


function showMessageBox() {
	const electron = require("electron");
	const remote = electron.remote;
	var dialog = remote.dialog;
	dialog.showMessageBox({
		type: 'info',
		buttons: ["OK"],
		title: "test",
		message: "test2",
		detail: "test3"
	}, (a,b,c)=>{
		console.log('test', a,b,c );
	});
};

function saveFile(callback) {
	if (window.currentFileName) {
		saveToFile(window.currentFileName, callback);
	} else {
		saveAsFile(callback);
	}
}

function saveAsFile(callback) {
	const electron = require("electron");
	const remote = electron.remote;
	var dialog = remote.dialog;
	var path = dialog.showSaveDialog({
		filters: [{
			name: "VisualScript", extensions: ["vs"]
		}]
	});
	console.log(path);
	saveToFile(path, callback);	
}

function saveToFile(filePath, callback) {
	if (filePath) {
		const fs = require('fs');
		var data = JSON.stringify(save());
		fs.writeFile(filePath, data, {encoding: "utf8"}, function() {
			window.currentFileName = filePath;
			setActiveTabTitle(filePath);
			if (typeof callback == 'function') {
				callback();
			}
		});	
	} else {
		console.log('File path is empty');
	}	
}

function loadFile() {
	const electron = require("electron");
	const remote = electron.remote;
	var dialog = remote.dialog;
	var path = dialog.showOpenDialog({
		filters: [{
			name: "VisualScript", extensions: ["vs"]
		}]
	});
	if (path && path.length > 0) {
		const fs = require('fs');
		var filePath = path[0];
		if (filePath) {
			fs.readFile(filePath, {encoding: "utf8"} , (err, data) => {
				var metaData = JSON.parse(data);
				load(metaData);
				window.currentFileName = filePath;
				setActiveTabTitle(filePath);
			});	
		}				
	}
}

function load(saveData) {
	blockIdSequece_ = 1;
	edgeSequence_ = 1;
	
	var canvas = getEl("canvas");	
	$(canvas).find(".block-component").remove();
	$(canvas).find("path").remove();
	// block component
	var blockComponentArray = saveData.node;
	for (var i = 0; i < blockComponentArray.length; i++) {
		var metaData = blockComponentArray[i];
		var block = createBlockComponent(canvas, metaData, metaData.offsetLeft, metaData.offsetTop);
	}
	// connection edge
	var connectionArray = saveData.connections;
	for (var i = 0; i < connectionArray.length; i++) {
		var metaData = connectionArray[i];
		createCurve(metaData.start, metaData.finish, metaData.type);
	}
}

var startX = 0;
var startY = 0;
function test(targetId) {
	for(var i = 0 ; i < 100; i++) {
		var block = createBlockComponent(getEl("canvas"), {
	    	"type": "variable",
	    	"name": "IntArray",
	    	"dataType": "int-array",
	    	"tooltip": "Variable IntArray.",
	    	"detailData": [{
	    		"name": "0",
	    		"type": "text",
	    		"value": "10"
	    	},{
	    		"name": "1",
	    		"type": "text",
	    		"value": "20"
	    	},{
	    		"name": "2",
	    		"type": "text",
	    		"value": "50"
	    	},{
	    		"name": "3",
	    		"type": "text",
	    		"value": "40"
	    	},{
	    		"name": "4",
	    		"type": "text",
	    		"value": "30"
	    	}]
	    }, startX++, startY++);
		createCurve($(block).find(".output-edge")[0].id, targetId);
	}
}

function setActiveTabTitle(filePath) {
	var activeTabLink = $('#page-wrapper .nav-tabs > li.active > a');
	var fileName = filePath.substring(filePath.lastIndexOf('\\') + 1);
	// mouse over tooltip file path
	// html file name
	activeTabLink.attr('title', filePath).html(fileName);	
}

function generateCode(contractTitle) {
	var a = new Generator();
	var b = save();
	var code = a.getCode(contractTitle, b.node, b.connections);
	showConsole(code);
	return code;
}

// Editor
Editor = {
	compiler: {
		fileName: 'scc.exe',
		args: ['-c'],
		folder: (__dirname + '/scm/').replace(/\\/g, '/')
		//folder: 'C:/etri/shifter/'
	},
	debuger: {
		fileName: 'scvmdebug.exe',
		args: [],
		folder: (__dirname + '/scm/').replace(/\\/g, '/'),
		//folder: 'C:/etri/shifter/',
		url: 'http://localhost:65500/',
		continue: 'continue',
		status: 'status',
		quit: 'quit',
		getvars: 'getvars',
		breaks: 'breaks',
		break: 'break'
	},
	breakpoints: []
};

Editor.init = function() {	
	this.fs = require('fs');
	this.process = require('child_process');

	// debug controller    
	$('#debugController > span').on('mousedown', function(event) {
		event.preventDefault();
		event.stopPropagation();
		event.returnValue = false;
		return false;
	});
	$('#continueBtn').on('click', function(event) {
		Editor.continueDebugger();
		return false;
	});
	$('#pauseBtn').on('click', function(event) {
		Editor.pauseDebugger();
		return false;
	});
	$('#stepOverBtn').on('click', function(event) {
		Editor.stepOverDebugger();
		return false;
	});
	$('#stepIntoBtn').on('click', function(event) {
		Editor.stepIntoDebugger();
		return false;
	});
	$('#stepOutBtn').on('click', function(event) {
		Editor.stepOutDebugger();
		return false;
	});
	$('#restartBtn').on('click', function(event) {
		Editor.restartDebugger();
		return false;
	});
	$('#stopBtn').on('click', function(event) {
		Editor.stopDebugger();
		return false;
	});
}

// Compile
Editor.compile = function(callback) {

	Editor.resetDebugger();

	// check saved file
	var path = window.currentFileName;
	if (!path) {
		saveAsFile(Editor.compile);
		return;
	}
	
	// generate code
	var filePath = path.substring(0, path.lastIndexOf("\\"));
	var fileName = path.substring(path.lastIndexOf("\\"), path.lastIndexOf('.'));
	var code = generateCode(fileName);
	
	// save scheme code
	var currentCodePath = filePath + fileName + '.scm';	
	this.fs.writeFile(currentCodePath, code, {encoding: "utf8"});
	console.log(currentCodePath);
	
	// start message
	this.showConsole('Start compile.', true);

	// compile
	// shifter.exe -c %1.scm
	let _args_ = this.compiler.args.slice();
	_args_.push(currentCodePath);
	this.process.execFile(
		this.compiler.fileName, _args_, {cwd: this.compiler.folder}, function(error, stdout, stderr) {
			if (error || stdout || stderr) {
				Editor.showConsole('Compiler message : ' + (error || stdout || stderr));
			}
			Editor.showConsole('Complete compile.');
			if (typeof callback == 'function') {
				callback(filePath + fileName);
			}
		}
	);
}

Editor.playDebugger = function(debugMode) {
	// debug mode
	this.showDebugMode();

	// compile
	this.compile(function(defaultName) {

		// setting
		let _debuger_ = Editor.debuger;
		let _args_ = _debuger_.args.slice();
		_args_.push(defaultName + '.out');

		// start message
		Editor.showConsole('Start debugger.');

		var spawn = require('child_process').spawn,
		ls    = spawn(_debuger_.folder + _debuger_.fileName, _args_);

		ls.stdout.on('data', function (data) {
			console.log('stdout');
			Editor.showConsole('Debugger message : ' + data);
		});

		ls.stderr.on('data', function (data) {
			console.log('stderr');
			Editor.showConsole('Debugger message : ' + data);
		});

		ls.on('exit', function (code) {
			console.log('exit');
			Editor.showConsole('Debugger message : ' + code);
		});


		// vm -boot %1.out
		Editor.process.execFile(_debuger_.fileName, _args_, {cwd: _debuger_.folder}, function(error, stdout, stderr) {
			if (error || stdout || stderr) {
				Editor.showConsole('debug : ' + (error || stdout || stderr));
			}
			Editor.showConsole('Finish debugger');			
		});
		Editor.setDebugger()
		Editor.continueDebugger();
		Editor.checkBreak();
	})
}
Editor.setDebugger = function() {
	currentValue = 0;
	for (var i = 0; i < this.breakpoints.length; i ++) {
		$.ajax({
			url: this.debuger.url + this.debuger.breaks + "/" + (i+1),
			async: false,
			success: function(response) {
				console.log(response);
			}
		});
	}
}

Editor.stopDebugger = function() {
	this.quitDebuger();
	this.hideDebugMode();
}

Editor.showDebugMode = function() {
	this.temp = {};

	// side menu
	$('#sideMenu').hide();

	// inspector
	$('#detail-wrapper').hide();
	
	// controller
	$('#debugController').show();

	// wrapper
	var _wrapper_ = $('#page-wrapper');
	this.temp['wrapper-margin-left'] = _wrapper_.css('margin-left');
	this.temp['wrapper-margin-right'] = _wrapper_.css('margin-right');
	_wrapper_.css('margin', 0);

	// tab
	$('.nav-tabs').hide();
	
	// workspace
	$('#canvasView').css('height', '100vh');
	
	// console
	$('#consoleView').hide();
	
	// debug watch
	$('#debugViewWrapper').css('display', 'flex').css('position', 'absolute')
		.css('right', 0).css('top', '0').css('width', '500px')
		.css('height', '100vh');
}

Editor.hideDebugMode = function() {
	// side menu
	$('#sideMenu').show();

	// inspector
	$('#detail-wrapper').show();

	// controller
	$('#debugController').hide();

	// wrapper
	var _wrapper_ = $('#page-wrapper');
	// _wrapper_.css('margin-left', this.temp['wrapper-margin-left']);
	// _wrapper_.css('margin-right', this.temp['wrapper-margin-right']);	
	_wrapper_.css('margin-left', '220px');
	_wrapper_.css('margin-right', '300px');	

	// tab
	$('.nav-tabs').show();

	// workspace
	$('#canvasView').css('height', 'calc(100vh - 200px)');
	
	// console	
	$('#consoleView').show();

	$('#debugViewWrapper').hide();
}

var checkTimeout;
var outCheckbreak;
var currentIndex = -1;

Editor.resetDebugger = function() {
	checkTimeout = null;
	outCheckbreak = null;
	currentIndex = -1;
}

Editor.getBreaks = function() {
	$.ajax({
		url: this.debuger.url + this.debuger.breaks,
		success: function(response) {
			console.log(response);
		}
	});
}

Editor.checkBreak = function() {
	Editor.getDebuggerStatus(function(result) {
		if (result) {			
			$.ajax({
				url: Editor.debuger.url + Editor.debuger.getvars + "/breakIndex",
				success: function(response) {
					index = response;
					var nodeId = Editor.breakpoints[currentIndex];
					if (nodeId) {
						console.log(nodeId);
						Editor.addBreak($('#' + nodeId));
						clearTimeout(outCheckbreak);
					}
				},
				error: function(response) {
					var nodeId = Editor.breakpoints[currentIndex];
					Editor.addBreak($('#' + nodeId));
					clearTimeout(outCheckbreak);
				}
			});
		} else {
			outCheckbreak = setTimeout(function() {
				clearTimeout(checkTimeout);
				Editor.quitDebuger();
			}, 2000);
		}
		checkTimeout = setTimeout(Editor.checkBreak, 1000);
	});
}

Editor.continueDebugger = function() {
	this.showConsole('continue');
	// $.ajax({
	// 	url: this.debuger.url + this.debuger.continue,
	// 	success: function(response) {
	// 		console.log(response);
	// 		if (Editor.breakpoints.length > (currentIndex + 1)) {
	// 			currentIndex ++;
	// 		}
	// 	}
	// });
	if (Editor.breakpoints.length > (currentIndex + 1)) {
		console.log(currentIndex, currentValue);
		currentIndex ++;
		currentValue += 10;
	} else {
		Editor.quitDebuger();
	}
}

Editor.stepOverDebugger = function() {
	this.showConsole('step over');
	$.ajax({
		url: this.debuger.url + this.debuger.continue,
		success: function(response) {
			console.log(response);
		}
	});
}

Editor.stepIntoDebugger = function() {
	this.showConsole('step into');
	$.ajax({
		url: this.debuger.url + this.debuger.continue,
		success: function(response) {
			console.log(response);
		}
	});
}

Editor.getDebuggerStatus = function(callback) {
	$.ajax({
		url: this.debuger.url + this.debuger.status,
		async: false,
		success: function(response) {
			console.log(response);
			var isBreak = (response == 'sleep');
			if (typeof callback == 'function') {
				callback(isBreak);
			}	
		}
	});
}

Editor.quitDebuger = function() {
	$('.breaked').removeClass('breaked');
	Editor.resetDebugger();
	Editor.showConsole('Quit debugger')
	$.ajax({
		url: this.debuger.url + this.debuger.quit
	});
}

Editor.stepOutDebugger = function() {
	this.showConsole('step out');
}

Editor.restartDebugger = function() {
	this.showConsole('restart');
	this.playDebugger();
}

Editor.pauseDebugger = function() {
	this.showConsole('pause');
}

Editor.toggleBreakpoint = function(item) {	
	if (!item) {
		item = $(".block-component.active");
		if (!item) {
			Editor.showConsole('Toggle Breakpoint require selected visual block.');
			return;
		}
	} else {
		item = $(item);
	}	
	if (item.hasClass('break-component')) {
		// remove
		item.removeClass('break-component');
		item.find('.break-point').remove();
		// push
		item[0].metaData.isBreakpoint = false;
		var index = this.breakpoints.indexOf(item[0].id);
		this.breakpoints.splice(index, 1);
	} else {
		// add
		item.addClass('break-component');
		var span = document.createElement('span');
		span.className = 'break-point';
		item.append(span);
		item[0].metaData.isBreakpoint = true;
		this.breakpoints.push(item[0].id);
	}
}

Editor.addBreak = function(item) {
	$('.breaked').removeClass('breaked');
	if (!item) {
		item = $(".block-component.active");
	}
	$(item).addClass('breaked');
	this.watchVariable();
}

Editor.watchVariable = function() {
	var data = save();
	var watch = $('#watchList').html('');
	for (var i = 0; i < data.node.length; i++) {
		let _node_ = data.node[i];
		if (_node_.type == NodeType.Variable) {
			$.ajax({
				url: this.debuger.url + this.debuger.getvars + "/" + _node_.variableName,
				success: function(response) {
					console.log('getvars', response);
					_node_.value = currentValue;
				},
				error: function() {
					_node_.value = currentValue;
				}
			});
			let _item_ = this.createWatchItem(_node_);
			watch.append(_item_);
		}
	}
}

Editor.createWatchItem = function(node) {
	var li = document.createElement('li');
	li.className = 'watch-item';
	
	// variable
	var variable = document.createElement('span');
	variable.className = 'watch-variable';
	variable.innerHTML = node.variableName;

	// value
	var value = document.createElement('span');
	value.className = 'watch-value variable-' + node.variableType.toLowerCase();	
	value.innerHTML = node.value || node.initValue;

	li.appendChild(variable);
	li.appendChild(value);
	return li;
}

Editor.showConsole = function(message, reset) {
	var html = (reset)?  "": $('.console-pre').html();
	html += message + "\r\n";
	// tab to white space
	html.replace(/\t/g, "    ");
	$('.console-pre').html(html);
	var el2 = $('.console-pre')[1];
	el2.scrollTop = el2.scrollHeight;
}

$(window).ready(function() {
	Editor.init();
});

window.onerror = function ( message, filename, lineno, colno, error ){
	console.log(message, filename, lineno, colno, error);
	showConsole('[' + filename + ']:['+ lineno + ':' + colno + '] Error: ' + error);
	return false;
}