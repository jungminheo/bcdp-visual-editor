/**
 * Common Function
 */
function createEl(tagName) {
	if (tagName == "svg" || tagName == "path") {
		return document.createElementNS("http://www.w3.org/2000/svg", tagName);
	} else {
		return document.createElement(tagName);	
	}	
}

function getEl(id) {
	return document.getElementById(id);
}

function addEl(parentEl, childEl) {
	parentEl.appendChild(childEl);
}

function addEvent(target, event, callback) {
	target.addEventListener(event, callback);
}

function createSelectOption(options) {
	// select
	var select = createEl('select');
	for (var i = 0; i < options.length; i ++) {		
		var option = createEl('option');
		option.value = options[i];
		option.innerHTML = options[i];
		select.appendChild(option);
	}
	return select
}