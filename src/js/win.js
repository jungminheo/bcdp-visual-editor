/**
 * windows option 
 */
const title_ = "Smart Contract Visual Lanaguage";

$(document).ready(()=>{
	// module
	const electron = require("electron");
	const remote = electron.remote;
	// resize window
	window.win_ = remote.getCurrentWindow();
	win_.setResizable(true);
	// screen ratio
	var ratio = 0.8;
	var positionRatio = 0.1;
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	win_.setBounds({
		x		: screenWidth * positionRatio,
		y		: screenHeight * positionRatio,
		width	: screenWidth * ratio,
		height	: screenHeight * ratio
	});
	setWindowTitle("");
});

function setWindowTitle(title) {
	win_.setTitle(title_);
}