/**
 * Main menu
 */
$(document).ready(()=>{
	createMenu();
});

function createMenu() {
	const electron = require('electron');
	const remote = electron.remote;
	const Menu = remote.Menu;	
	const template = [
			{
				label: 'File',
				submenu: [
				{
				label: 'Save',
				accelerator: 'CmdOrCtrl+S',
				click (item, focusedWindow) {
					saveFile();
					}
			},
					{
						label: 'Save As',
							accelerator: 'CmdOrCtrl+Shift+S',
							click (item, focusedWindow) {
								saveAsFile();
							}
					},
					{
						label: 'Load',
							accelerator: 'CmdOrCtrl+O',
							click (item, focusedWindow) {
								loadFile();
							}
					}
				]
			},
			{
				label: 'View',
				submenu: [
					{
						label: 'Reload',
						accelerator: 'CmdOrCtrl+R',
						click (item, focusedWindow) {
							if (focusedWindow) focusedWindow.reload()
						}
					},
					{
						label: 'Toggle Developer Tools',
						accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
						visible: false,
						click (item, focusedWindow) {
							e.preventDefault();
							return false;
						}
					}
				]
			},{
				label: 'Debug',
				submenu: [{
					label: 'Compile',
					accelerator: 'CmdOrCtrl+F5',
					click (item, focusedWindow) {
						// 컴파일러
						Editor.compile();
					}
				}, {
					label: 'Start Debugging',
					accelerator: 'F5',
					click (item, focusedWindow) {
						Editor.playDebugger();
					}
				}, {
					type: 'separator'				
				}, {
					label: 'Continue',
					accelerator: 'F9',
					click (item, focusedWindow) {
						Editor.continueDebugger();
					}
				}, {
					label: 'Step Over',
					accelerator: 'F10',
					click (item, focusedWindow) {
						Editor.stepOverDebugger();
					}
				}, {
					label: 'Step Into',
					accelerator: 'F11',
					click (item, focusedWindow) {
						Editor.stepIntoDebugger();
					}
				}, {
					label: 'Step Out',
					accelerator: 'Shift+F11',
					click (item, focusedWindow) {
						Editor.stepOutDebugger();
					}
				}, {
					label: 'Restart',
					accelerator: 'Ctrl+Shift+F5',
					click (item, focusedWindow) {
						Editor.restartDebugger();
					}
				},{
					label: 'Stop',
					accelerator: 'Shift+F5',
					click (item, focusedWindow) {
						Editor.stopDebugger();
					}
				}, {
					type: 'separator'				
				}, {
					label: 'Toggle Breakpoint',
					accelerator: 'F8',
					click (item, focusedWindow) {
						Editor.toggleBreakpoint();
					}
				}, {
					label: 'Break Example',					
					click (item, focusedWindow) {
						Editor.addBreak();
					}
				}]
			}
		]

	const menu = Menu.buildFromTemplate(template)
	Menu.setApplicationMenu(menu);
}

