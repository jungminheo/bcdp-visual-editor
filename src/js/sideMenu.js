/**
 * side menu
 */
$(document).ready(()=>{
	// read block.json
	const fs = require('fs');
	fs.readFile(__dirname + "/block.json", {encoding: "utf8"} , (err, data) => {
		var data = JSON.parse(data);
		var blocks = data.blocks;
		if (blocks) {
			createMainMenu(blocks);	
		}		
	});
	window.treeLevel_ = 2;
});

function createMainMenu(data) {
	// device
//	createBlockLists(getEl("event"),     data["Event"]);
	createBlockLists(getEl("function"),  data["Function"], true);
	createBlockLists(getEl("arithmetic"),data["Arithmetic Operation"]);
	createBlockLists(getEl("arithmetic2"),data["Arithmetic Operation2"]);
	createBlockLists(getEl("boolean"),	 data["Boolean Operation"]);
	createBlockLists(getEl("comparison"),data["Comparison Operation"]);
	createBlockLists(getEl("array"),	 data["Array Operation"]);
	createBlockLists(getEl("control"),	 data["Control Block"]);
	createBlockLists(getEl("stableVariable"),	 data["Variable"], true);
	
	createBlockLists(getEl("literal"),	 data["Literal"], true);
	
	collaseMenu();
}

function createBlockLists(el, data, subSet) {
	createMenuItems(el, data, subSet);
	treeLevel_ = 2;
}

function createMenuItems(parentEl, arrData, subSet) {
	if (arrData) {
		// ul
		var list = createMenuList();
		addEl(parentEl, list);
		// li
		for(var i = 0; i < arrData.length; i++) {
			var itemData = arrData[i];
			var item = createMenuItem(itemData);			
			addEl(list, item);
			// subItems
			var tempTreeLevel = treeLevel_;
			if (itemData.items) {
				treeLevel_ ++;
				var link = $(item).find("a");
				// collapse arrow
				if (link.length > 0) {					
					var arrow = createEl("span");
					arrow.className = "fa arrow";
					addEl(link[0], arrow);
				}				
				createMenuItems(item, itemData.items);				
			}	
			treeLevel_ = tempTreeLevel;
		}
		
		if (subSet) {
			var ulEl = createEl("ul");
			ulEl.className = "nav nav-subset collapse";
			addEl(parentEl, ulEl);
		}
	}
}

function addMenuItem(ul, arrData, id) {
	if (arrData) {
		var item = createMenuItem(arrData);
		item.id = id;
		addEl(ul, item);
		return item;
	}
}

function collaseMenu() {
	$(".metismenu li").on("click", (e) => {
		var target = e.currentTarget;		
		// check ul collapse inside li
		var collapse = $(target).children(".in").length;
		if (collapse == 0) {
			// check other main list and active
			$(target.parentNode).children("li").children("ul").removeClass("in");			
			$(target).children("ul").addClass("in");
			
			// block type list not active
			if ($(target).children("a")[0].metaData && $(target).children("a")[0].metaData.type == "block") {
				
			} else {				
				$(target.parentNode).children(".active").removeClass("active")
				$(target).addClass("active");
			}			
		} else {
			$(target).children("ul").removeClass("in");
		}
		return false;
	});
}

function createMenuList() {
	var ulEl = createEl("ul");
	ulEl.className = "nav nav-" + treeLevel_ + "-level collapse";
	return ulEl;
}

function createMenuItem(metaData) {
	var listEl = createEl("li"); 
	var linkEl = createEl("a");
	linkEl.innerHTML = metaData.name;
	linkEl.metaData = metaData;	
	addEl(listEl, linkEl);
	// check sublist
	if(!metaData.items) {
		// drag event
		linkEl.draggable="true";
		linkEl.addEventListener("dragstart", (e) => {
			e.dataTransfer.setData("metaData", JSON.stringify(e.target.metaData));
			e.dataTransfer.setData("type", "create");		
		});		
	}
	return listEl;
}