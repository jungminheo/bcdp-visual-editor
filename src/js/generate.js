/**
 * 
 */
var NodeType = {
		Event : 'customFunction',
		Function : 'function',
		Constructor: 'constructor',
		Operater : 'block',
		Variable: 'variable',
		Conditionals: 'conditionals',
		Let: 'let'
}

var EdgeType = {
		Event : 'event',
		Param : 'param'
}

function Generator() {
	this.indent = 0;
	this.nullValue = 'None';
}

Generator.prototype.getCode = function(title, nodes, edges) {
	// 노드 (BOX) 맵
	this.nodeMap = {};	
	// 전체 입/출력 노드 (이벤트, 파라미터 입출력 노드) 맵
	this.edgeNodeMap = {};
	// 노드간 연결 정보 (이벤트, 파라미터 입출력 연결) 맵
	this.edgeMap = {
		event: {
			// from : to
		},
		param: {
			// to : from
		}
	};
	// 변수 선언 노드
	var variableNode = [];
	// 커스텀 함수(이벤트) 노드
	var eventNode = [];
	// 일반 함수 호출 노드
	var functionNode = [];
	// 생성자 노드
	var constructor;
	
	// 노드 정보를 바틍으로 데이터 생성
	for (var i = 0; i < nodes.length; i++) {
		var node = nodes[i];
		this.nodeMap[node.nodeId] = node;
		if (node.type == NodeType.Event) {
			// event
			eventNode.push(node);
		} else if (node.type == NodeType.Constructor) {
			constructor = node;
		} else if (node.type == NodeType.Variable) {
			variableNode.push(node);
		} else if (node.type == NodeType.Function || node.type == NodeType.Conditionals || node.type == NodeType.Let) {
			// 함수 호출일 경우
			// sink 노드 찾기			
			let sinkNode;			
			for (var edge in node.edge) {
				if (node.edge[edge].type == 'sink') {
					sinkNode = edge;
					break;
				}
			}
			if (sinkNode) {
				// 연결 찾기
				let _edge_;
				for (var j = 0; j < edges.length; j ++) {
					if (edges[j].finish == sinkNode) {
						_edge_ = edges[j];
						break;
					}
				}
				if (!_edge_) {
					// 연결이 없으면 추가
					functionNode.push(node);
				}
			}			
		}
		
		for (var nodeId in node.edge) {
			this.edgeNodeMap[nodeId] = node.edge[nodeId];
		}
	}
	
	// edge to map
	for (var i = 0; i < edges.length; i++) {
		var edge = edges[i];
		if (edge.type == EdgeType.Event) {
			// function flow edge
			this.edgeMap.event[edge.start] = edge.finish;
		} else if (edge.type == EdgeType.Param) {
			// parameter flow edge
			this.edgeMap.param[edge.finish] = edge.start;
		}			
	}
	this.variableNode = variableNode;
	//var code = '(include "shifter.scm")\n';
	var code = '(define breakIndex 0)\n';
	// code += 'class ' + title + ':\r\n';
	// this.indent += 4;
	
	// member variable
	for (var i = 0; i < variableNode.length; i++) {
		code += this.getBlockCode(variableNode[i]);
	}
	
	// constructor
	// if (!constructor) {
	// 	constructor = {type:'constructor'};
	// }
	// code += this.getBlockCode(constructor);
	
	// generate custom procedure code
	// 사용자 정의 함수 코드 생성
	for (var i = 0; i < eventNode.length; i++) {
		code += this.getBlockCode(eventNode[i]);	
	}

	// generate procedure call code
	// 일반 함수 호출 코드 생성
	for (var i = 0; i < functionNode.length; i++) {
		code += this.getBlockCode(functionNode[i]);	
	}

	return code;
}
Generator.prototype.getIndent = function() {
	var code = '';
	for (var i = 0; i < this.indent; i++) {
		code += ' ';
	}
	return code;
}

var DataType = {
	//'Int', 'String', 'Boolean', 'Array', 'Map'
	Integer: 'Int',
	String: 'String',
	Boolean: 'Boolean',
	Array: 'Array',
	Map: 'Map'
}

Generator.prototype.getVariableCode = function(variable) {
	var _type_ = variable.variableType;
	var code = "";
	if (_type_ == DataType.Integer) {
		// (define x 2)
		code += this.getIndent() + "(define "  + variable.variableName + " " + (variable.initValue || 0)+ ")\n";
	} else if (_type_ == DataType.String) {
		// (define x "test")
		code += this.getIndent() + "(define "  + variable.variableName + " \"" + (variable.initValue || "")+ "\")\n";
	} else if (_type_ == DataType.Boolean) {
		// (define x #t)
		code += this.getIndent() + "(define "  + variable.variableName + " " + ((variable.initValue)? "#t":"#f") + ")\n";
	} else if (_type_ == DataType.Array) {
		// (define x (list ’a ’b ’c))
		console.log(variable);
		var _dim1_ = variable.dimension1 || 0;
		var _dim2_ = variable.dimension2 || 0;
		var _dim3_ = variable.dimension3 || 0;		
		
		code += this.getIndent() + "(define " + variable.variableName + "\n";
		if (_dim3_ != 0 && _dim2_ != 0 && _dim1_ != 0) {
			// (make-list 3 (make-list 2 (make-list 3 0))))
			code += this.getIndent() + "\t(make-list " + _dim3_ + " (make-list " + _dim2_ + " (make-list " + _dim1_ + " 0))))\n";
		} else if (_dim2_ != 0 && _dim1_ != 0) {
			code += this.getIndent() + "\t(make-list " + _dim2_ + " (make-list " + _dim1_ + " 0)))\n";
		} else if (_dim1_ != 0) {
			code += this.getIndent() + "\t(make-list " + _dim1_ + " 0))\n";
		} else {
			return "";
		}
		
		// (array-ref array index0 ...)
		// (list-set! array value index0 ...) 
		for (var i = 0; i < _dim1_; i++) {
			if (_dim2_ == 0) {
				// 1 dimension array
				var _value_ = variable.initValue[i];
				code += this.getIndent() + "(list-set! " + variable.variableName + " " + i + " " + _value_ + ")\n";
				continue;
			}
			for (var j = 0; j < _dim2_; j++) {
				if (_dim3_ == 0) {
					// 2 dimension array
					var _value_ = variable.initValue[j][i];
					code += this.getIndent() + "(list-set! (array-ref! " + variable.variableName + " " + i + ") " + j + " " + _value_ + ")\n";
					continue;
				}
				for (var k = 0; k < _dim3_; k++) {
					// 3 dimension array
					var _value_ = variable.initValue[k][j][i];
					code += this.getIndent() + "(list-set! (array-ref! (array-ref! " + variable.variableName + " " + i + ") " + j + ") " + k + " " + _value_ + ")\n";
				}
			}
		}
	}
	// code += this.getIndent() + "(display " + variable.variableName + ")\n";
	return code;
}

Generator.prototype.getNextCode = function(node) {
	code = '';
	// check next
	if (node.outEventEdge) {
		if (node.outEventEdge instanceof Array) {
			for (var i = 0; i < node.outEventEdge.length; i ++) {
				var targetEdgeId = this.edgeMap.event[node.outEventEdge[i].portId];
				if (targetEdgeId) {
					var edge = this.edgeNodeMap[targetEdgeId];
					if (edge) {
						var targetNodeId = edge.nodeId
						if (targetNodeId) {
							var targetNode = this.nodeMap[targetNodeId];	
							console.log(targetNodeId, targetNode);
							code += this.getBlockCode(targetNode);		
						}			
					}
				} else if (node.type == NodeType.Constructor || node.type == NodeType.Event) {
					code += this.getIndent() + ')';
				}					
			}
		} else {
			var targetEdgeId = this.edgeMap.event[node.outEventEdge.portId];
			if (targetEdgeId) {
				var edge = this.edgeNodeMap[targetEdgeId];
				if (edge) {
					var targetNodeId = edge.nodeId
					if (targetNodeId) {
						var targetNode = this.nodeMap[targetNodeId];	
						console.log(targetNodeId, targetNode);
						code += this.getBlockCode(targetNode);		
					}			
				}
			} else if (node.type == NodeType.Constructor || node.type == NodeType.Event) {
				code += this.getIndent() + 'pass\r\n';
			}			
		}
	} else if (node.type == NodeType.Constructor || node.type == NodeType.Event) {
		code += this.getIndent() + 'pass\r\n';
	}
	
	if (node.type == NodeType.Constructor || node.type == NodeType.Event) {
		this.indent -= 4;
	}
	return code;
}

Generator.prototype.getBlockCode = function(node) {
	var code = '';
	if (node.isBreakpoint && node.breakIndex != -1) {
		for (var i = 0; i < this.variableNode.length; i++) {
			var _variable_ = this.variableNode[i]
			code += '($debug-var "' + _variable_.variableName + '" '+ _variable_.variableName + ')\n';
		}
		code += '(set! breakIndex ' + node.breakIndex + ')\n';
		code += '($debug-var "breakIndex" breakIndex)\n';
		code += '($debug-break ' + (node.breakIndex + 1) + ')\n';
	}
	
	if (node.type == NodeType.Constructor) {
		// function define
		code += this.getIndent() + 'def __init__(self):\r\n';
		this.indent += 4;
	}
	
	if (node.type == NodeType.Variable) {
		if (!node.isGlobal) {			
			code += this.getVariableCode(node)
		}
	}
	
	if (node.type == NodeType.Event) {
		// function define		
		// check input parameters
		var param = [];
		if (node.output) {
			for (var i = 0; i < node.output.length; i++) {
				if (node.output[i]) {
					param.push(node.output[i].name);					
				}
			}

			// define function
			code += this.getIndent() + "(define " + node.name + "\n";
			this.indent += 4;
			// lambda
			if (param.length > 0) {				
				code += this.getIndent() + "(lambda (" + param.join(' ') + ")\n"				
			} else {
				code += this.getIndent() + "(lambda ()\n"
			}			
			this.indent += 4;
			// body
			code += this.getNextCode(node);			
			// lambda close			
			code += this.getIndent() + ')\n';
			// function close
			this.indent -= 4;			
			code +=  this.getIndent() + ')\n';
		} else {
			code += this.getIndent() + '(define (' + node.name + ')\n';
			this.indent += 4;
			code += this.getNextCode(node);
			code += ')';
		}		
	}
	
	if (node.type == NodeType.Function) {
		// check generate function
		var fn = node.fn;
		if (node.native) {
			// native function

		} else {
			// custom function call
			var args = [];
			for (var i = 0; i < node.input.length; i++) {
				args.push("{" + (i + 1) + "}");
			}
			fn = "(" + node.name + " " + args.join(" ") + ")";
		}

		// input
		for (var i = 0; i < node.input.length; i++) {
			var value = this.getFieldValue(node.input[i]);
			var exp = new RegExp('\\\{' + (i + 1) + '\\\}', 'g');
			fn = fn.replace(exp, value);
		}

		// return value
		if (node.output) {
			var returnEdgeNode = node.output[0];
			// exists return value
			var returnRef = [returnEdgeNode.portId, 'return'];			
			this.edgeNodeMap[returnEdgeNode.portId].name = returnRef.join('_');			
			code += this.getIndent() + "(define " + returnRef.join('_') + " " + fn + ")\n";
		} else {
			code += this.getIndent() + fn + "\n";
		}

		code += this.getNextCode(node);
	}
	
	if (node.type == NodeType.Operater) {
		var fn = node.fn;
		if (fn && node.input) {
			// input
			for (var i = 0; i < node.input.length; i++) {
				var value = this.getFieldValue(node.input[i]);
				var exp = new RegExp('\\\{' + (i + 1) + '\\\}', 'g');
				fn = fn.replace(exp, value);
			}
		}
		code = fn;
	}

	// Conditionals (ex. IF)
	if (node.type == NodeType.Conditionals) {
		var fn = node.fn;
		if (fn && node.input) {
			// input
			for (var i = 0; i < node.input.length; i++) {
				var value = this.getFieldValue(node.input[i]);
				var exp = new RegExp('\\\{' + (i + 1) + '\\\}', 'g');
				fn = fn.replace(exp, value);
			}
			if (node.outEventEdge && node.outEventEdge.length == 2) {
				// true condition	
				var trueNode = this.getNextNode(node.outEventEdge[0]);
				if (trueNode) {
					var trueCondition = this.getBlockCode(trueNode);					
					fn = fn.replace('{#t}', trueCondition);
				}
				var falseNode = this.getNextNode(node.outEventEdge[1]);				
				if (falseNode) {
					var falseCondition = this.getBlockCode(falseNode);					
					fn = fn.replace('{#f}', falseCondition);
				}
			}			
		}
		code = fn;
	}

	// let or let*
	if (node.type == NodeType.Let) {
		var fn = node.fn;
		if (fn && node.input) {
			// input
			for (var i = 0; i < node.input.length; i++) {
				var value = this.getFieldValue(node.input[i]);
				var exp = new RegExp('\\\{' + (i + 1) + '\\\}', 'g');				
				fn = fn.replace(exp, value);
				if (i == 0) {
					// local variable name
					node.output[0].name = value;
				}
			}
			if (node.outEventEdge && node.outEventEdge.length == 1) {
				// body
				var bodyNode = this.getNextNode(node.outEventEdge[0]);
				if (bodyNode) {
					var bodyCode = this.getBlockCode(bodyNode);					
					fn = fn.replace('{body}', bodyCode);
				}				
			}			
		}
		code = fn;
	}
	return code;
}

Generator.prototype.getFieldValue = function(input) {
	var targetEdgeId = this.edgeMap.param[input.portId];
	var targetEdge = this.edgeNodeMap[targetEdgeId];
	var value;
	if (targetEdge) {
		var targetNode = this.nodeMap[targetEdge.nodeId];
		if (targetNode.type == NodeType.Operater) {
			value = this.getBlockCode(targetNode);
		} else {
			value = targetEdge.name;
		}
	} else {
		// check input value
		var inputValue = $('#' + input.portId).parent().find('.input-value');
		if (input.type == "text") {
			value = inputValue.val();
			value = (value)? ('"' + value + '"'):"";
		} else if (input.type == "boolean") {
			value = inputValue.is(':checked');
			value = (value === true)? "#t":"#f";
		} else if (input.type == "int" || input.type == "float") {
			value = inputValue.val();
			value = (isNaN(value))? 0:value;
		} else {
			value = inputValue.val();
		}
	}
	return value;
}

Generator.prototype.getNextNode = function(outEventEdge) {
	var targetEdgeId = this.edgeMap.event[outEventEdge.portId];
	if (targetEdgeId) {
		var edge = this.edgeNodeMap[targetEdgeId];
		if (edge) {
			var targetNodeId = edge.nodeId
			if (targetNodeId) {
				var targetNode = this.nodeMap[targetNodeId];
				return targetNode;				
			}			
		}
	}
	return null;					
}